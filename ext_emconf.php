<?php

# Extension Manager/Repository config file for ext: "t3themes_materializecss"

$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Themes - Materializecss',
    'description' => 'View demo: https://materializecss.fenewmedia.de/ - TYPO3 Theme Integration for EXT:t3cms. You can integrate the "ready-to-use html content" with fluid content, dce, flux, mask, ckeditor or something else.',
    'category' => 'distribution',
    'author' => 'Salvatore Eckel',
    'author_email' => 'salvaracer@gmx.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.4.1',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.18-9.99.99',
            't3cms' => '2.0.1'
        ],
        'conflicts' => [],
        'suggests' => [
            't3content_materializecss' => '2.3.0',
        ],
    ],
];
