<?php
defined('TYPO3_MODE') || die();

/***************
 * Default TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('t3themes_materializecss', 'Configuration/TypoScript', 'TYPO3 Themes - Materializecss');
