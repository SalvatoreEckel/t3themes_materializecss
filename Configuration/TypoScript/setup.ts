<INCLUDE_TYPOSCRIPT: source="DIR:EXT:t3themes_materializecss/Configuration/TypoScript/Setup/" extensions="ts,txt,typoscript">

#### PAGE
page = PAGE
page {
    typeNum = 0
    #shortcutIcon = {$website.shortcutIcon}
    10 = FLUIDTEMPLATE
    10 {
        templateName = TEXT
        templateName.stdWrap.cObject = CASE
        templateName.stdWrap.cObject {
            key.data = pagelayout

            pagets__materializecssclean = TEXT
            pagets__materializecssclean.value = Default

            default = TEXT
            default.value = Default
        }

        templateRootPaths.0 = EXT:t3themes_materializecss/Resources/Private/Templates/Page/
        partialRootPaths.0 = EXT:t3themes_materializecss/Resources/Private/Partials/Page/
        layoutRootPaths.0 = EXT:t3themes_materializecss/Resources/Private/Layouts/Page/

        ### DATA PREPROCESSING
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            10 {
                levels = 2
                includeSpacer = 1
                as = mainnavigation
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                entryLevel = 1
                levels = 2
                expandAll = 0
                includeSpacer = 1
                as = subnavigation
            }
        }

        ### VARIABLES
        variables {
            pageTitle = TEXT
            pageTitle.data = page:title
            rootPage = TEXT
            rootPage.data = leveluid:0
        }
    }
    meta {
        viewport = width=device-width, initial-scale=1, shrink-to-fit=no
        keywords.data = DB:pages:1:keywords 
        keywords.override.field = keywords 
        description.data = DB:pages:1:description 
        description.override.field = description 
        abstract.data = DB:pages:1:abstract 
        abstract.override.field = abstract 
        author.data = DB:pages:1:author 
        author.override.field = author
        web_author.data = DB:pages:1:author 
        web_author.override.field = author
    }

    #includeCSSLibs {}
    includeCSS {
        materialize = EXT:t3themes_materializecss/Resources/Public/css/materialize.css
        style = EXT:t3themes_materializecss/Resources/Public/css/style.css
    }
    #includeJSLibs {}
    includeJSFooterlibs {
        jquery = https://code.jquery.com/jquery-2.1.1.min.js
        jquery.external = 1
        jquery.forceOnTop = 1
        materialize = EXT:t3themes_materializecss/Resources/Public/js/materialize.js
        init = EXT:t3themes_materializecss/Resources/Public/js/init.js
    }
    #jsFooterInline {}
    headerData {
		12 = TEXT
        12.value = <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    }
}

[userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('t3cms')]
page.10.dataProcessing.30 = SalvatoreEckel\T3cms\DataProcessing\T3themesConfProcessor
page.10.dataProcessing.30.fieldName = t3themes_conf
page.10.dataProcessing.30.as = t3themesConf
page.10.dataProcessing.30.rootpageId = TEXT
page.10.dataProcessing.30.rootpageId {
    insertData = 1
    data = leveluid : 0
}
[global]
