# EXT:t3themes_materializecss #

This is a ready to use template for TYPO3. Together with EXT:t3cms you have an execelnt website now!

### General Information ###

* t3themes_materializecss
* v1.4.1
* Resources/Public : http://materializecss.com/templates/parallax-template/preview.html

### How do I get set up? ###

* Install the extension from TER
* Make sure EXT:t3cms is installed
* Include static templates and pageTS from t3cms & t3themes_materializecss

* Have fun with your page settings and backend layouts. Check out the backend module.

### Contribution guidelines ###

* We are happy, when you contribute with us!
* Merge requests are wished,
* PSR-2 guidlines

### Who do I talk to? ###

* Salvatore Eckel
* salvaracer@gmx.de